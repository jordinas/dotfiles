# ~/.bash_logout

[[ -n $XDG_CACHE_HOME ]] && . ~/bin/xdg_cache_home.sh

# vim:ts=4:sw=4:syntax=sh:
