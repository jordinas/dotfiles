# .bashrc

# Check duplicated dot files
if [[ -e ~/.bash_profile ]]; then
	echo 1>&2 'WARNING: ~/.bash_profile hides ~/.bash_login'
fi

# Source global definitions
[[ -f /etc/bashrc ]] && . /etc/bashrc

########################################################################
# aliases
########################################################################

if (( $UID == 0 )); then
	alias rm='rm -i'
	alias cp='cp -i'
	alias mv='mv -i'
fi

alias bc='bc -q'
alias cx='chmod +x'
alias l="cls; command ls --color --group-directories-first --classify --format=single-column | nl"
alias mailx='mutt -x'
alias mc='TERM=xterm . /usr/libexec/mc/mc-wrapper.sh'
alias psc='ps xawf -eo pid,user,cgroup,args'
alias pstree='pstree -U'
alias vi='vim'
alias whence='type -a'

case $TERM in
	linux|xterm*|screen*)
		alias cls="echo -en '\e[0m\e[1;1H\e[2J'"
		alias reset="echo -en '\ec'"
	;;
	*)
		alias cls="clear"
	;;
esac

########################################################################
# Options
########################################################################

set +o histexpand
set +o ignoreeof
set -o noclobber

shopt -s autocd
shopt -s globstar
shopt -s extglob

if (( $UID != 0 )); then
	set -o vi
fi

########################################################################
# Variables
########################################################################

FCEDIT='/bin/vi -u NONE'

PYTHONSTARTUP=~/.pythonrc

#CDPATH=.:..:~

#TMOUT=$(( 3600 * 1 ))

########################################################################
# Functions
########################################################################

# print (better echo)
function print { IFS=' ' printf '%b\n' "$*"; } 

# cd with bookmarks
function cdb {
	if (( $# == 0 )); then
		[[ -e ~/.cdb ]] && cat ~/.cdb | sed '/^#/d;/^$/d'
	elif [[ -d $1 ]]; then
		builtin cd "$@"
	elif [[ $1 == - ]]; then
		builtin cd -
	elif [[ -e ~/.cdb ]]; then
		local directory=$(
			sed 's/#.*//;s/[ \t]\+$//;s/^[ \t]//;/^$/d' ~/.cdb |
				while read bookmark directory; do
					if [[ $1 == $bookmark ]]; then
						print "$directory"
						break
					fi
				done
		)
		if [[ -n $directory ]]; then
			builtin cd "$directory"
			print "$PWD"
		else
			builtin cd "$@"
		fi
	else
		builtin cd "$@"
	fi
}

# better help
function help {
	if (( $# == 0 )); then
		builtin help | less
	else 
		case $1 in
		help)	# redefined
			builtin help "$@" | less
		;;
		git)
			shift
			git help "$@" | less
		;;
		tmux)
			shift
			if (( $# == 0 )); then
				tmux list-commands | grep --color=always '^[^ ]\+' | less -r
			else
				tmux list-commands | grep "^$1"
			fi
		;;
		-d)	# description
			case $(type -t $2) in
			builtin|keyword)
				builtin help "$@"
			;;
			function)
				[[ $2 == help ]] && builtin help "$@"
			;;
			*)	shift
				whatis "$@"
			esac
		;;
		-s)	# synopsis for builtins
			builtin help "$@"
		;;
		*) 	case $(type -t $1) in
			builtin|keyword)
				builtin help -m "$@" | less
			;;
			*)	man "$@"
			esac
		esac
	fi
}

#
function wanted {
	local unit=$1
	systemctl show -p "Wants" $unit |
		sed 's/^[^=]\+=//' |
		tr ' ' '\n' |
		sort
}

function required {
	local unit=$1
	systemctl show -p "Requires" $unit |
		sed 's/^[^=]\+=//' |
		tr ' ' '\n' |
		sort
}

# vim:ts=4:sw=4:syntax=sh:
