export XDG_CACHE_HOME=/var/tmp/$USER/cache
[ -d $XDG_CACHE_HOME ] || mkdir --mode 700 --parents $XDG_CACHE_HOME
