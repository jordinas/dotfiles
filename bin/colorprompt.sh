# to put in /etc/profile.d

[ -n "$BASH" ] || return

function _color_prompt
{
	# ISO 6429 character sequences for colors etc
	# LCS = leading character sequence, common for all colors.
	local -r LCS='\[\e[1' ECS='\]'

	# foregrounds    			backgrounds
	local -r BLACK="$LCS;30m"   B_BLACK="$LCS;40m"
	local -r RED="$LCS;31m"     B_RED="$LCS;41m"
	local -r GREEN="$LCS;32m"   B_GREEN="$LCS;42m"
	local -r YELLOW="$LCS;33m"  B_YELLOW="$LCS;43m"
	local -r BLUE="$LCS;34m"    B_BLUE="$LCS;44m"
	local -r PURPLE="$LCS;35m"  B_PURPLE="$LCS;45m"
	local -r CYAN="$LCS;36m"    B_CYAN="$LCS;46m"
	local -r WHITE="$LCS;37m"   B_WHITE="$LCS;47m"

	# effects
	local -r BRIGHT="$LCS;1m"
	local -r UNDER="$LCS;4m"
	local -r FLASH="$LCS;5m"

	# set the prompt color
	local PC
	local RC="$LCS;0m$ECS"	# reset character
	if [[ $TERM  == dumb ]]; then
		unset RC	# no color if a dumb terminal
	elif [[ $UID == 0 ]]; then
		PC="$RED$ECS"
	else
		PC="$PURPLE$ECS"
	fi

	PS1="$PC\u@\h \W\\\$ $RC"
	#PS1="$PC[\u@\h \w]\\\$ $RC"
	#PS1="$PC[\u@\h \W]\\\$ $RC"
}

_color_prompt

unset -f _color_prompt

# vim:syntax=sh:ai:sw=4:ts=4
