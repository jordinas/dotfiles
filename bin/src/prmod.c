/* prmod.c - lista ficheros con sus 'modo' octal
 * $Id: prmod.c,v 1.2 2002/12/31 17:40:04 joan Exp joan $
 * Joan Ordinas <jordinas@escoladeltreball.org>
 */

/* Ejemplo de uso:
 * 	paste <( awk '{print $1}' <( ls -l | sed 1d ) ) <( prmod )
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	struct stat st;
	char buf[256];
	int i;

	if (argc == 1) {
		const char* sh = getenv("SHELL");
		if (sh == NULL) {
			perror(argv[0]);
			return 1;
		}
		snprintf(buf, sizeof(buf)-1, "%s *", argv[0]);
		execl(sh, sh, "-c", buf, (char*)NULL);
		/* "casi" imposible llegar aqui... */
		perror(argv[0]);
		return 2;
	}
	/* else  argc > 1 */
	for (i = 1; i < argc; ++i) {
		if (stat(argv[i], &st) == 0) {
			fprintf(stdout, "%04o\t%s\n", (unsigned)st.st_mode&07777, argv[i]);
		} else {
			snprintf(buf, sizeof(buf)-1, "%s: %s", argv[0], argv[i]);
			perror(buf);
		}
	}

	return 0;
}
