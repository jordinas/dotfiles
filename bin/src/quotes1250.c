
#include <stdio.h>

main()
{
	int c;
	while ((c = getchar()) != EOF) {
		switch (c) {
			case 0x92:
				putchar('\'');
				break;
			case 0x93:
				putchar('"');
				break;
			case 0x94:
				putchar('"');
				break;
			default:
				putchar(c);
		}
	}
}
