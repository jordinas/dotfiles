" .vimrc

" Options
set autoindent
set guifont=Monospace\ 11
"set guioptions-=M
"set guioptions-=m
"set guioptions-=T
set guioptions-=R
set guioptions-=r
set guioptions-=L
set guioptions-=l
set hlsearch
set laststatus=2
set showmode
set showcmd
set linebreak
set matchpairs+=<:>
set modeline
set more
"set mouse=a
set nocompatible
"set noruler
set noshowmatch
"set nosmarttab
set noswapfile
set noterse
set nowrap
set number
set report=0
set shiftround
set shiftwidth=8
set shortmess+=I
"set statusline=%<%f%y%m%r\ \ %F%=%b\ 0x%B\ \ %l,%c%V\ %P
set tabstop=8
set viminfo+=n~/.viminfo
set visualbell
set wrapscan

" *** CUA ***
behave mswin
" backspace and cursor keys wrap to previous/next line
set backspace=indent,eol,start whichwrap+=<,>,[,]
" backspace in Visual mode deletes selection
vnoremap <BS> d
" SHIFT-Del is Cut
vnoremap <S-Del> "+x
" CTRL-Insert is Copy
vnoremap <C-Insert> "+y
" SHIFT-Insert is Paste
map <S-Insert> "+gP
cmap <S-Insert> <C-R>+
imap <S-Insert> <S-Insert>
vmap <S-Insert> <S-Insert>
" CTRL-Tab is Next window
noremap <C-Tab> <C-W>w
inoremap <C-Tab> <C-O><C-W>w
cnoremap <C-Tab> <C-C><C-W>w
onoremap <C-Tab> <C-C><C-W>w

filetype indent on
syntax on

"colorscheme elflord
"colorscheme evening
"colorscheme desert

" Macros
map ' `

" Function keys
map <F2> :edit $HOME/.vimrc
map <F12> :source $HOME/.vimrc
map <F3> \be
map <F4> 
map <F5> :make
map <F7> :previous
map <F8> :next
map <F9> :w
" in insert mode
map! <F9> <F9>

" syntax=vim:fileencoding=UTF-8
